package com.example.abdo.simpleapp.ui.home.presenter;

import android.util.Log;

import com.example.abdo.simpleapp.data.DataRepository;
import com.example.abdo.simpleapp.data.models.PhotoDetail;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Abdo on 20/09/2018.
 */

public class PhotosPresenterImp implements PhotosContract.Presenter{
    private final DataRepository mDataRepository;
    private final PhotosContract.View mView;

    @Inject
    void setupListeners() {
        mView.setPresenter(this);
    }
    @Inject
    public PhotosPresenterImp(DataRepository dataRepository, PhotosContract.View mView) {
        this.mDataRepository = dataRepository;
        this.mView = mView;
    }
    @Override
    public void getPhotosByType(final String type) {
        mDataRepository.getPhotos(type, mDataRepository.getACCESS_KEY()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<PhotoDetail>>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        mView.showError();
                    }

                    @Override
                    public void onNext(List<PhotoDetail> response) {
                        Log.d("hello", "response : "+response);
                        if (type.equals("latest"))
                            mView.photosLatestSucceded(response);
                        else
                            mView.photosPopularSucceded(response);
                    }
                });
    }

    @Override
    public void getRandomPhotos() {
        mDataRepository.getRandomPhotos(mDataRepository.getACCESS_KEY()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<PhotoDetail>>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                        mView.showError();
                    }

                    @Override
                    public void onNext(List<PhotoDetail> response) {
                        mView.photosRandomSucceded(response);
                    }
                });
    }
}
