package com.example.abdo.simpleapp.data;

import com.example.abdo.simpleapp.data.source.remote.Remote;
import com.example.abdo.simpleapp.data.source.remote.RemoteDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Abdo on 19/09/2018.
 */

@Module
public class DataRepositoryModule {
    @Singleton
    @Provides
    @Remote
    DataSource provideDataRepository() {
        return new RemoteDataSource();
    }
}
