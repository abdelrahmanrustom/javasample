package com.example.abdo.simpleapp;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.example.abdo.simpleapp.data.DaggerDataRepositoryComponent;
import com.example.abdo.simpleapp.data.DataRepositoryComponent;
import com.example.abdo.simpleapp.data.DataRepositoryModule;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

/**
 * Created by Abdo on 19/09/2018.
 */

public class SimpleApp extends MultiDexApplication {
    private DataRepositoryComponent mDataRepositoryComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
        mDataRepositoryComponent = DaggerDataRepositoryComponent.builder()
                .appModule(new AppModule(getApplicationContext()))
                .dataRepositoryModule(new DataRepositoryModule())
                .build();
    }

    public DataRepositoryComponent getDataRepositoryComponent() {
        return mDataRepositoryComponent;
    }
}
