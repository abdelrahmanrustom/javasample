package com.example.abdo.simpleapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import com.example.abdo.simpleapp.receivers.NetworkStatusReceiver;
import com.example.abdo.simpleapp.ui.splash.SplashActivity;
import com.example.abdo.simpleapp.utils.Networkmanager;
import com.example.abdo.simpleapp.utils.Utils;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by Abdo on 19/09/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements NetworkStatusReceiver.NetworkStatusListener{
    public ProgressDialog progress;
    private NetworkStatusReceiver networkStatusReceiver;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        if(!(this instanceof SplashActivity))
            overridePendingTransition(R.anim.slide_in_right, R.anim.stay);
    }

    @Override
    protected void onResume() {
        addConnectivityListener();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        removeConnectivityListener();
    }

    void addConnectivityListener() {
        networkStatusReceiver = new NetworkStatusReceiver();
        registerReceiver(networkStatusReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }
    void removeConnectivityListener() {
        unregisterReceiver(networkStatusReceiver);
    }
    void initViews() {
        //Assuming that this app requires th network to be available all the time
        networkAlertDialog();
    }

    void networkAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.check_interent_connection).setTitle(R.string.network_connection);
        builder.setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startChecking();
                    }
                }, 1000);
            }
        });
        builder.setCancelable(false);
        AlertDialog networkDialog = builder.create();
    }

    public void startChecking() {
        if (!Networkmanager.isOnline(this))
        Utils.ShowSnackBar(this, getString(R.string.no_interent_connection), Utils.SNACK_BAR_ACTION_NETWROK_SETTING);
        else {
            whenAllIsGood();
            Utils.HideReloadBar();
        }
    }

    @Override
    public void onNetworkFail() {
        checkingNetworkAndGPS();
    }

    @Override
    public void onNetworkConnected() {
        checkingNetworkAndGPS();
    }

    public void checkingNetworkAndGPS() {
        if (Networkmanager.isOnline(this) ) {
            whenAllIsGood();
            Utils.HideReloadBar();

        } else {
            Utils.ShowSnackBar(this, getString(R.string.no_interent_connection), Utils.SNACK_BAR_ACTION_NETWROK_SETTING);
        }
    }

    /*
  * super function when all listeners are ok
  * which means it will be called when internet is working to do something
  * in any activity that extend this class
   */
    @CallSuper
    public void whenAllIsGood() {
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}
