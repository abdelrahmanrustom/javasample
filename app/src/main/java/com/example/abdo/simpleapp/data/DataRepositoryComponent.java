package com.example.abdo.simpleapp.data;

import com.example.abdo.simpleapp.AppModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Abdo on 19/09/2018.
 */

@Singleton
@Component(modules = {AppModule.class, DataRepositoryModule.class})
public interface DataRepositoryComponent {
    DataRepository getDataRepository();
}

