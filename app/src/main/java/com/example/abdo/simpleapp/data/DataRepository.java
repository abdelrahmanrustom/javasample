package com.example.abdo.simpleapp.data;

import android.content.Context;

import com.example.abdo.simpleapp.data.models.PhotoDetail;
import com.example.abdo.simpleapp.data.source.remote.Remote;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;

/**
 * Created by Abdo on 19/09/2018.
 */
@Singleton
public class DataRepository implements DataSource{
    private DataSource mRemoteDataSource;
    private Context mContext;
    private final String ACCESS_KEY = "d4bf21d580f8d147137df42f360a46af1da999e4d3294936a90d6e5eb2c6972b";
    @Inject
    public DataRepository(@Remote DataSource dataSource, Context context) {
        this.mRemoteDataSource = dataSource;
        this.mContext = context;
    }

    @Override
    public Observable<List<PhotoDetail>> getPhotos(String order_By, String client_id) {
        return mRemoteDataSource.getPhotos(order_By, client_id);
    }

    @Override
    public Observable<List<PhotoDetail>> getRandomPhotos(String client_id) {
        return mRemoteDataSource.getRandomPhotos(client_id);
    }

    public String getACCESS_KEY() {
        return ACCESS_KEY;
    }
}
