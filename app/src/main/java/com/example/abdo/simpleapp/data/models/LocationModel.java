package com.example.abdo.simpleapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abdo on 20/09/2018.
 */

public class LocationModel {
    @SerializedName("title")
    String title;

    @SerializedName("country")
    String country;

    public LocationModel(){

    }
    public String getTitle() {
        return title;
    }

    public String getCountry() {
        return country;
    }
}
