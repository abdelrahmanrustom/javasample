package com.example.abdo.simpleapp.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import com.example.abdo.simpleapp.ui.home.view.PhotosActivity;
import com.example.abdo.simpleapp.ui.photo_details.PhotoDetails;

/**
 * Created by Abdo on 19/09/2018.
 */

public class NavigationManager {

    public static void goToMainActivity(Context context) {
        Intent intent = new Intent(context, PhotosActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
    public static void goToPhotoDetails(Context context, Parcelable parcelable) {
        Intent intent = new Intent(context, PhotoDetails.class);
        if (parcelable != null){
            intent.putExtra("photo", parcelable);
        }
        context.startActivity(intent);
    }
}
