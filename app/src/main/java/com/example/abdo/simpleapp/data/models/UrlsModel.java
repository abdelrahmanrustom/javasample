package com.example.abdo.simpleapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abdo on 20/09/2018.
 */

public class UrlsModel {
    @SerializedName("full")
    String full;

    @SerializedName("regular")
    String regular;

    @SerializedName("small")
    String small;

    public UrlsModel(){

    }
    public String getFull() {
        return full;
    }

    public String getRegular() {
        return regular;
    }

    public String getSmall() {
        return small;
    }
}
