package com.example.abdo.simpleapp.ui.home.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.abdo.simpleapp.R;
import com.example.abdo.simpleapp.SimpleApp;
import com.example.abdo.simpleapp.adapters.PhotosAdapter;
import com.example.abdo.simpleapp.data.models.PhotoDetail;
import com.example.abdo.simpleapp.ui.home.di.DaggerPhotosComponent;
import com.example.abdo.simpleapp.ui.home.di.PhotosModule;
import com.example.abdo.simpleapp.ui.home.presenter.PhotosContract;
import com.example.abdo.simpleapp.utils.Utils;
import com.example.abdo.simpleapp.utils.views.Animations;
import com.example.abdo.simpleapp.utils.views.SpacesItemDecoration;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A placeholder fragment containing a simple view.
 */

public class PhotosActivityFragment extends Fragment implements PhotosContract.View{
    @Inject
    PhotosContract.Presenter mPhotoPresenter;
    @BindView(R.id.latest_recycle)
    android.support.v7.widget.RecyclerView latest_recycle;
    @BindView(R.id.popular_recycle)
    android.support.v7.widget.RecyclerView popular_recycle;
    @BindView(R.id.random_recycle)
    android.support.v7.widget.RecyclerView random_recycle;
    @BindView(R.id.latestTextView)
    TextView latestTextView;
    @BindView(R.id.popularTextView)
    TextView popularTextView;
    @BindView(R.id.randomTextView)
    TextView randomTextView;
    @BindView(R.id.view)
    ConstraintLayout view;
    @BindView(R.id.progressBarLatest)
    ProgressBar progressBarLatest;
    @BindView(R.id.progressBarRandom)
    ProgressBar progressBarRandom;
    @BindView(R.id.progressBarPopular)
    ProgressBar progressBarPopular;

    private PhotosAdapter mPhotoAdapterLatest;
    private PhotosAdapter mPhotoAdapterPopular;
    private PhotosAdapter mPhotoAdapterRandom;
    private Unbinder unbinder;

    public PhotosActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerPhotosComponent.builder()
                .dataRepositoryComponent(((SimpleApp) getActivity().getApplication()).getDataRepositoryComponent())
                .photosModule(new PhotosModule(this))
                .build()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photos, container, false);
        unbinder = ButterKnife.bind(this,view);
        initView();
        return view;
    }

    void initView(){
        if(mPhotoPresenter != null) {
            mPhotoPresenter.getPhotosByType("latest");
            mPhotoPresenter.getPhotosByType("popular");
            mPhotoPresenter.getRandomPhotos();
        }
        mPhotoAdapterLatest = new PhotosAdapter(getContext());
        initRecycleViews(mPhotoAdapterLatest,latest_recycle);
        mPhotoAdapterPopular = new PhotosAdapter(getContext());
        initRecycleViews(mPhotoAdapterPopular,popular_recycle);
        mPhotoAdapterRandom = new PhotosAdapter(getContext());
        initRecycleViews(mPhotoAdapterRandom,random_recycle);
        Animations.animateTextViews(latestTextView, getString(R.string.latest_photos), 200);
        Animations.animateTextViews(popularTextView, getString(R.string.popular_photos), 200);
        Animations.animateTextViews(randomTextView, getString(R.string.random_photos), 200);
    }

    private void animateView(final ProgressBar progressBar, final RecyclerView recyclerView, int duration) {
        // view is 0% opacity but visible
        recyclerView.setAlpha(0f);
        recyclerView.setVisibility(View.VISIBLE);
        // Animate the view to 100% opacity, and clear animations on the view.
        recyclerView.animate()
                .alpha(1f)
                .setDuration(duration)
                .setListener(null);
        // Animate progress to 0% opacity then visibility to GONE
        progressBar.animate()
                .alpha(0f)
                .setDuration(duration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }


    void initRecycleViews(PhotosAdapter adapter, android.support.v7.widget.RecyclerView recyclerView){
        recyclerView.setAdapter(mPhotoAdapterPopular);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new SpacesItemDecoration(Utils.getIntInDB(getContext(), 11), Utils.getIntInDB(getContext(), 11), Utils.getIntInDB(getContext(), 11), Utils.getIntInDB(getContext(), 11), 0));
        recyclerView.setAdapter(adapter);
    }
    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void showError() {

    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void photosLatestSucceded(List<PhotoDetail> photosResponses){
        animateView(progressBarLatest, latest_recycle, 100);
        mPhotoAdapterLatest.getAllPhotos().addAll(photosResponses);
        mPhotoAdapterLatest.notifyDataSetChanged();

    }

    @Override
    public void photosPopularSucceded(List<PhotoDetail> photosResponses){
        animateView(progressBarPopular, popular_recycle, 100);
        mPhotoAdapterPopular.getAllPhotos().addAll(photosResponses);
        mPhotoAdapterPopular.notifyDataSetChanged();
    }

    @Override
    public void photosRandomSucceded(List<PhotoDetail> photosResponses) {
        animateView(progressBarRandom, random_recycle, 100);
        mPhotoAdapterRandom.getAllPhotos().addAll(photosResponses);
        mPhotoAdapterRandom.notifyDataSetChanged();
    }

    //this one is used in activities to be accessed across its fragments
    @Override
    public void setPresenter(PhotosContract.Presenter presenter) {

    }
}
