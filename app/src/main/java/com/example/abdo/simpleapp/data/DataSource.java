package com.example.abdo.simpleapp.data;

import com.example.abdo.simpleapp.data.models.PhotoDetail;

import java.util.List;

import rx.Observable;

/**
 * Created by Abdo on 19/09/2018.
 */

public interface DataSource {
    Observable<List<PhotoDetail>> getPhotos(String client_id, String order_By);
    Observable<List<PhotoDetail>> getRandomPhotos(String client_id);

}
