package com.example.abdo.simpleapp.ui.home.di;

import com.example.abdo.simpleapp.data.DataRepositoryComponent;
import com.example.abdo.simpleapp.ui.home.view.PhotosActivityFragment;
import com.example.abdo.simpleapp.utils.FragmentScoped;

import dagger.Component;

/**
 * Created by Abdo on 20/09/2018.
 */
@FragmentScoped
@Component(dependencies = DataRepositoryComponent.class, modules = PhotosModule.class)
public interface PhotosComponent {
    void inject(PhotosActivityFragment fragment);
}
