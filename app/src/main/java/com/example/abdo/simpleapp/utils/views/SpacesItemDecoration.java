package com.example.abdo.simpleapp.utils.views;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Abdo on 20/09/2018.
 */

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int rightSpace, leftSpace, bottomSpace, topSpace, extraTopSpace;
    public SpacesItemDecoration(int rightSpace, int leftSpace, int bottomSpace, int topSpace, int extraTopSpace) {
        this.rightSpace = rightSpace;
        this.leftSpace = leftSpace;
        this.bottomSpace = bottomSpace;
        this.topSpace = topSpace;
        this.extraTopSpace = extraTopSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        // add margin right to every item
        outRect.right = rightSpace;
        if (parent.getChildAdapterPosition(view) == 0) {
            // add margin to first item only
            outRect.left = leftSpace;
            if (topSpace != 0)
                outRect.top = topSpace + extraTopSpace;
        } else {
            outRect.left = 0;
            // outRect.top = topSpace;
            if (topSpace != 0)
                outRect.top = topSpace;
        }
        // last item
        if (parent.getChildAdapterPosition(view) == state.getItemCount() - 1) {
                outRect.right = rightSpace + bottomSpace;
                outRect.bottom = 0;
        } else {
            outRect.bottom = 0;
        }
    }
}