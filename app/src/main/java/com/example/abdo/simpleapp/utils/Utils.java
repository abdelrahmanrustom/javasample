package com.example.abdo.simpleapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import com.example.abdo.simpleapp.R;

/**
 * Created by Abdo on 19/09/2018.
 */

public class Utils {

    private static Snackbar mSnackbar;
    public static int SNACK_BAR_ACTION_NETWROK_SETTING = 101;
    public static final String PRE_URL = "https://api.unsplash.com/";
    public static void ShowSnackBar(final Activity activity, String message, final int actionType) {
        mSnackbar = Snackbar
                .make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (actionType == SNACK_BAR_ACTION_NETWROK_SETTING) {
                            activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        }
                    }
                });
// Changing message text color
        mSnackbar.setActionTextColor(Color.RED);
// Changing action button text color
        View sbView = mSnackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark));
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        mSnackbar.show();
    }
    public static int getIntInDB(Context context, int value) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.getResources().getDisplayMetrics());
    }
    public static void HideReloadBar() {
        if (mSnackbar != null)
            mSnackbar.dismiss();
    }
}
