package com.example.abdo.simpleapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abdo.simpleapp.R;
import com.example.abdo.simpleapp.data.models.PhotoDetail;
import com.example.abdo.simpleapp.ui.home.presenter.PhotosContract;
import com.example.abdo.simpleapp.ui.viewholders.PhotoViewHolder;

import java.util.ArrayList;

/**
 * Created by Abdo on 20/09/2018.
 */

public class PhotosAdapter extends RecyclerView.Adapter<PhotoViewHolder>{
    private Context mContext;
    private ArrayList<PhotoDetail> allPhotos;

    public PhotosAdapter(Context context ) {
        mContext = context;
        allPhotos = new ArrayList<>();
    }


    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.photo_item, parent, false);
        PhotoViewHolder viewHolder = new PhotoViewHolder(v, mContext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {
        holder.bind(this, position);
    }

    @Override
    public int getItemCount() {
        return allPhotos.size();
    }

    public ArrayList<PhotoDetail> getAllPhotos() {
        return allPhotos;
    }
}
