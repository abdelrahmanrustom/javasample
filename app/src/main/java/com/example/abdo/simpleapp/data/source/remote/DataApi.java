package com.example.abdo.simpleapp.data.source.remote;

import com.example.abdo.simpleapp.data.models.PhotoDetail;

import java.util.List;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Abdo on 19/09/2018.
 */

public interface DataApi {
    @GET("photos")
    Observable<List<PhotoDetail>> getPhotos( @Query("order_by") String order_by, @Query("client_id") String client_id);

    @GET("photos/random")
    Observable<List<PhotoDetail>> getRandomPhotos( @Query("count") String count, @Query("client_id") String client_id);
}


