package com.example.abdo.simpleapp.ui.viewholders;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.abdo.simpleapp.R;
import com.example.abdo.simpleapp.adapters.PhotosAdapter;
import com.example.abdo.simpleapp.data.models.PhotoDetail;
import com.example.abdo.simpleapp.utils.NavigationManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Abdo on 20/09/2018.
 */

public class PhotoViewHolder extends RecyclerView.ViewHolder {
    private Context mContext;
    private Unbinder unbinder;

    @BindView(R.id.photo)
    @Nullable
    ImageView photo;

    @BindView(R.id.view)
    @Nullable
    ConstraintLayout view;
    AnimationDrawable animation;

    public PhotoViewHolder(View itemView, Context context) {
        super(itemView);
        mContext = context;
        unbinder =ButterKnife.bind(this, itemView);
        animation = (AnimationDrawable) view.getBackground();
        animation.setEnterFadeDuration(1000);
        animation.setExitFadeDuration(1000);
        animation.start();
    }

    public void bind(final PhotosAdapter adapter, int position) {
        final PhotoDetail item = (PhotoDetail) adapter.getAllPhotos().get(position);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavigationManager.goToPhotoDetails(mContext, item);
            }
        });
        //create listener to stop the animation aka shimmer when image is loaded
        Glide.with(mContext).load(item.getUrls().getSmall()).listener(new RequestListener<Drawable>() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                animation.stop();
                view.setBackground(null);
                return false;

            }

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                animation.stop();
                view.setBackground(null);
                return false;
            }
        })
                .into(photo);
    }
}
