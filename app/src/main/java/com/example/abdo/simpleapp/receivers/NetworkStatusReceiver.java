package com.example.abdo.simpleapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Abdo on 19/09/2018.
 */

public class NetworkStatusReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork)
        {
            int type = activeNetwork.getType();
            if (type == ConnectivityManager.TYPE_WIFI || type == ConnectivityManager.TYPE_MOBILE)
            {
                //its connected to internet :)
                ((NetworkStatusListener) context).onNetworkConnected();

            }
            else
                ((NetworkStatusListener) context).onNetworkFail();
        }
        else
            ((NetworkStatusListener) context).onNetworkFail();
    }

    public interface NetworkStatusListener
    {
        void onNetworkFail();
        void onNetworkConnected();
    }
}
