
package com.example.abdo.simpleapp.ui.photo_details;

import android.annotation.TargetApi;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.abdo.simpleapp.BaseActivity;
import com.example.abdo.simpleapp.R;
import com.example.abdo.simpleapp.data.models.PhotoDetail;
import com.example.abdo.simpleapp.utils.views.Animations;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PhotoDetails extends BaseActivity {
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.description_text)
    TextView description_text;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.location_text)
    TextView location_text;
    @BindView(R.id.likes)
    TextView likes;
    @BindView(R.id.views)
    TextView views;
    @BindView(R.id.downloads)
    TextView downloads;
    @BindView(R.id.linear_layout)
    LinearLayout linear_layout;
    @BindView(R.id.imageView)
    ImageView imageView;
    private AnimationDrawable mAnimation;
    private PhotoDetail mPhotoDetail;
    private Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);
        unbinder = ButterKnife.bind(this);
        getPassedData();
        initAnimation();
        initView();
    }

    private void initAnimation(){
        //animate imageview (Shimmer)
        mAnimation = (AnimationDrawable) imageView.getBackground();
        mAnimation.setEnterFadeDuration(1000);
        mAnimation.setExitFadeDuration(1000);
        mAnimation.start();
        //Animate textViews
        Animations.animateTextViews(description, getString(R.string.description), 200);
        Animations.animateTextViews(location, getString(R.string.location), 200);
    }
    private void initView(){

        if (mPhotoDetail != null){
            //create listener to stop the animation aka shimmer when image is loaded
            Glide.with(this).load(mPhotoDetail.getUrls().getFull()).listener(new RequestListener<Drawable>() {
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    mAnimation.stop();
                    imageView.setBackground(null);
                    return false;
                }
                @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    mAnimation.stop();
                    imageView.setBackground(null);
                    return false;
                }
            }).into(imageView);
            Animations.animateTextViews(description_text, mPhotoDetail.getDescription() != null? mPhotoDetail.getDescription(): "-", 200);
            Animations.animateTextViews(location_text, mPhotoDetail.getLocation().getCountry() != null? mPhotoDetail.getLocation().getCountry(): "-", 200);
            //set number of likes ...etc
            String numberLikes = getString(R.string.likes);
            numberLikes += mPhotoDetail.getLikes() != null? mPhotoDetail.getLikes(): "-";
            Animations.animateTextViews(likes, numberLikes, 200);
            String numberDownloads = getString(R.string.downloads);
            numberDownloads += mPhotoDetail.getDownloads() != null? mPhotoDetail.getDownloads(): "-";
            Animations.animateTextViews(downloads, numberDownloads, 200);
            String numberViews = getString(R.string.views);
            numberViews += mPhotoDetail.getViews() != null? mPhotoDetail.getViews(): "-";
            Animations.animateTextViews(views, numberViews, 200);

        }
    }
    private void getPassedData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPhotoDetail = extras.getParcelable("photo");
        }
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
