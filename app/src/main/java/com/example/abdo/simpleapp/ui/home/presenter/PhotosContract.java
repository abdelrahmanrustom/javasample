package com.example.abdo.simpleapp.ui.home.presenter;

import com.example.abdo.simpleapp.IBaseView;
import com.example.abdo.simpleapp.data.models.PhotoDetail;

import java.util.List;

/**
 * Created by Abdo on 20/09/2018.
 */

public interface PhotosContract {
    interface View extends IBaseView<Presenter> {
        void photosLatestSucceded(List<PhotoDetail> photosResponses);
        void photosPopularSucceded(List<PhotoDetail> photosResponses);
        void photosRandomSucceded(List<PhotoDetail> photosResponses);
        void showError();

        void showError(String error);
    }

    interface Presenter {
        void getPhotosByType(String type);
        void getRandomPhotos();
    }
}
