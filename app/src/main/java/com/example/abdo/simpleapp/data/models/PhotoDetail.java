package com.example.abdo.simpleapp.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abdo on 20/09/2018.
 */

public class PhotoDetail implements Parcelable {

    @SerializedName("id")
    String id;
    @SerializedName("description")
    String description;
    @SerializedName("urls")
    UrlsModel urls;
    @SerializedName("likes")
    String likes;
    @SerializedName("views")
    String views;
    @SerializedName("downloads")
    String downloads;
    @SerializedName("location")
    LocationModel location;

    public String getId() {
        return id;
    }

    public UrlsModel getUrls() {
        return urls;
    }

    public String getLikes() {
        return likes;
    }

    public String getViews() {
        return views;
    }

    public String getDownloads() {
        return downloads;
    }

    public LocationModel getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getLikes());
        parcel.writeString(getDownloads());
        parcel.writeString(getViews());
        parcel.writeString(getDescription());
        if (getUrls() != null)
            parcel.writeString(getUrls().getFull());
        if(getLocation() != null)
            parcel.writeString(getLocation().getCountry());

    }
    public static final Parcelable.Creator<PhotoDetail> CREATOR = new Parcelable.Creator<PhotoDetail>() {
        public PhotoDetail createFromParcel(Parcel in) {
            return new PhotoDetail(in);
        }

        public PhotoDetail[] newArray(int size) {
            return new PhotoDetail[size];
        }
    };
    private PhotoDetail(Parcel in) {
        likes = in.readString();
        downloads = in.readString();
        views = in.readString();
        description = in.readString();
        //because we only need the full image size from url model
        UrlsModel urlsModel = new UrlsModel();
        this.urls = urlsModel;
        urls.full = in.readString();
        // because we only need the country name from location model
        LocationModel locationModel = new LocationModel();
        this.location = locationModel;
        locationModel.country = in.readString();
    }
}
