package com.example.abdo.simpleapp.ui.splash;

import android.os.Handler;
import android.os.Bundle;

import com.example.abdo.simpleapp.BaseActivity;
import com.example.abdo.simpleapp.R;
import com.example.abdo.simpleapp.data.DataRepository;
import com.example.abdo.simpleapp.utils.NavigationManager;

public class SplashActivity extends BaseActivity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        launchActivity();

    }

    private void launchActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                NavigationManager.goToMainActivity(SplashActivity.this);
            }
        }, SPLASH_DISPLAY_LENGTH);

    }
}
