package com.example.abdo.simpleapp;

/**
 * Created by Abdo on 19/09/2018.
 */

public interface IBaseView<T> {
    void setPresenter(T presenter);

}