package com.example.abdo.simpleapp.utils.views;

import android.view.animation.AlphaAnimation;
import android.widget.TextView;

/**
 * Created by Abdo on 21/09/2018.
 */

public class Animations {
    public static void animateTextViews(final TextView textView, final String text, int duration){
        AlphaAnimation alphaAnimation1 = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation1.setDuration(duration);
        alphaAnimation1.setRepeatCount(1);
        alphaAnimation1.setRepeatMode(android.view.animation.Animation.REVERSE);
        alphaAnimation1.setAnimationListener(new android.view.animation.Animation.AnimationListener() {
            @Override
            public void onAnimationStart(android.view.animation.Animation animation) {
            }
            @Override
            public void onAnimationEnd(android.view.animation.Animation animation) {
            }

            @Override
            public void onAnimationRepeat(android.view.animation.Animation animation) {
                textView.setText(text);
            }
        });
        textView.startAnimation(alphaAnimation1);
    }
}
