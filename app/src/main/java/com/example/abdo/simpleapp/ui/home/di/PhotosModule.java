package com.example.abdo.simpleapp.ui.home.di;

import com.example.abdo.simpleapp.ui.home.presenter.PhotosContract;
import com.example.abdo.simpleapp.ui.home.presenter.PhotosPresenterImp;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Abdo on 20/09/2018.
 */
@Module
public class PhotosModule {
    private final PhotosContract.View mView;

    public PhotosModule(PhotosContract.View view) {
        this.mView = view;
    }

    @Provides
    PhotosContract.View provideLoginView() {
        return mView;
    }

    @Provides // <-- provide constructor injected stuff and bind to interface
    PhotosContract.Presenter registerAccountPresenter(PhotosPresenterImp impl) {
        return impl;
    }
}
