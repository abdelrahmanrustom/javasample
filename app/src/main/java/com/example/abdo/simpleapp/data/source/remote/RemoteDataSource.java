package com.example.abdo.simpleapp.data.source.remote;

import com.example.abdo.simpleapp.data.DataSource;
import com.example.abdo.simpleapp.data.models.PhotoDetail;

import java.util.List;

import rx.Observable;

/**
 * Created by Abdo on 19/09/2018.
 */

public class RemoteDataSource extends BaseRemoteDataSource implements DataSource {

    @Override
    public Observable<List<PhotoDetail>> getPhotos(String order_By, String client_id ) {
        return createService(DataApi.class).getPhotos(order_By,client_id);
    }

    @Override
    public Observable<List<PhotoDetail>> getRandomPhotos(String client_id) {
        return createService(DataApi.class).getRandomPhotos("10", client_id);
    }

}
